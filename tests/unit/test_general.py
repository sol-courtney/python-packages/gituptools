"""General unit test module."""

import unittest

import gituptools


class TestGeneral(unittest.TestCase):

    """General unit test cases."""

    def test_gitlab_dump(self):
        """Make a dump file for visual inspections."""
        gituptools.Gitlab.dump()

    def test_has_rtd_site(self):
        """Make sure the pypi parser works."""
        self.assertTrue(gituptools.has_rtd_site('gituptools'))
        self.assertFalse(gituptools.has_rtd_site('gituptoolsxx'))

    def test_get_version(self):
        """Make sure the pypi parser works."""
        v = gituptools.next_version('gituptoolsxx')
        self.assertEqual(v, '1')

    def test_Gitlab(self):
        """Make sure we can load static files."""
        self.assertIsInstance(gituptools.Gitlab.GITLAB_CI, str)
        self.assertIsInstance(gituptools.Gitlab.CI, str)
        self.assertTrue(gituptools.Gitlab)
        self.assertIsInstance(gituptools.Gitlab.kwargs, dict)
        self.assertIsNone(gituptools.Gitlab.RANDOM_ATTR)

    def test_setup(self):
        """Do a dry-run on the setup function."""
        kwargs = gituptools.setup(_dryrun=True)
        self.assertIsInstance(kwargs, dict)

    def test_version_parser(self):
        """Do a dry-run on the setup function."""
        check = gituptools.is_canonical_version
        self.assertTrue(check('1'))
        self.assertTrue(check('1.0'))
        self.assertTrue(check('1.0.0'))
        self.assertFalse(check('v1'))
